package Git;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class LogintoPage_Test extends utils {
	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "/GitAuthentication/Drivers/chromedriver.exe");

		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("--disable-blink-features=AutomationControlled", false);

		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("useAutomationExtension", true);
		options.addArguments("disable-infobars");
		options.addArguments("window-size=1280,800");
		options.addArguments(
				"user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3729.169 Safari/537.36");
		options.addArguments("--profile-directory=Default");
		options.addArguments("--incognito");
		options.addArguments("--disable-blink-features=AutomationControlled");

		options.merge(capabilities);
		ChromeDriver driver = new ChromeDriver(options);
		driver.get("https://gitlab.com/users/sign_in");
		driver.manage().deleteAllCookies();

		Thread.sleep(15000);
		utils.signin(driver,utils.get_username(),utils.get_password());
		utils.createProject(driver, "Testproject");
//		utils.createProject(driver," ");
//		utils.createProject(driver,"name3");
//		utils.createProject(driver,"name3");

	    driver.close();
	}

	protected static boolean isTextPresent(String text) {
		try {
			boolean b = driver.getPageSource().contains(text);
			return b;
		} catch (Exception e) {
			return false;
		}
	}

}

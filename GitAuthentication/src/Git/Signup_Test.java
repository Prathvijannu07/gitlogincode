package Git;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



public class Signup_Test {
	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "/GitAuthentication/Drivers/chromedriver.exe");

		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("--disable-blink-features=AutomationControlled", false);

		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("useAutomationExtension", true);
		options.addArguments("disable-infobars");
		options.addArguments("window-size=1280,800");
		options.addArguments(
				"user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3729.169 Safari/537.36");
		options.addArguments("--profile-directory=Default");
		options.addArguments("--incognito");
		options.addArguments("--disable-blink-features=AutomationControlled");

		options.merge(capabilities);
		ChromeDriver driver = new ChromeDriver(options);
		driver.get("https://gitlab.com/users/sign_up");
		driver.manage().deleteAllCookies();
		Thread.sleep(15000);
		WebElement element = driver.findElement(By.xpath(
				"//p[contains(text(),\"By clicking Register, I agree that I have read and accepted the GitLab\")]"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		driver.findElement(By.xpath("//a[contains(text(),'Terms of Use and Privacy Policy')]")).click();
		driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS); 
		
		ArrayList<String> tab = new ArrayList<>(driver.getWindowHandles());
		driver.switchTo().window(tab.get(1));
		driver.findElement(By.xpath("//*[@id=\"content-body\"]/div/div[2]/p[1]/a")).click();
		driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS); 

		ArrayList<String> tab2 = new ArrayList<>(driver.getWindowHandles());
		driver.switchTo().window(tab.get(2));
		driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS); 
		driver.findElement(By.cssSelector("#main-nav > div.navbar-right > ul > li:nth-child(8) > a")).click();
        
		// Since captcha verification is required , could not not complete remaining steps
		
	}

}
